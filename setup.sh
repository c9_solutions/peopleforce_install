#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

##
## Make sure only root can run our script
##
check_root() {
  if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root. Please sudo or log in as root first." 1>&2
    exit 1
  fi
}

##
## Check whether a connection to HOSTNAME ($1) on PORT ($2) is possible
##
connect_to_port () {
  HOST="$1"
  PORT="$2"
  VERIFY=$(date +%s | sha256sum | base64 | head -c 20)
  if ! [ -x "$(command -v nc)" ]; then
    echo "In order to check the connection to $HOST:$PORT we need to open a socket using netcat."
    echo However netcat is not installed on your system. You can continue without this check
    echo or abort the setup, install netcat and try again.
    while true; do
      read -p "Would you like to continue without this check? [yn] " yn
      case $yn in
      [Yy]*) return 2 ;;
      [Nn]*) exit ;;
      *) echo "Please answer y or n." ;;
      esac
    done
  else
    echo -e "HTTP/1.1 200 OK\n\n $VERIFY" | nc -w 4 -l -p $PORT >/dev/null 2>&1 &
    if curl --proto =http -s $HOST:$PORT --connect-timeout 3 | grep $VERIFY >/dev/null 2>&1; then
      return 0
    else
      curl --proto =http -s localhost:$PORT >/dev/null 2>&1
      return 1
    fi
  fi
}

check_IP_match() {
  HOST="$1"
  echo
  echo Checking your domain name . . .
  connect_to_port $HOST 443; ec=$?
  case $ec in
    0)
      echo "Connection to $HOST succeeded."
      ;;
    1)
      echo "WARNING: Port 443 of computer does not appear to be accessible using hostname:  $HOST."
      if connect_to_port $HOST 80; then
        echo
        echo SUCCESS: A connection to port 80 succeeds!
        echo This suggests that your DNS settings are correct,
        echo but something is keeping traffic to port 443 from getting to your server.
        echo Check your networking configuration to see that connections to port 443 are allowed.
      else
        echo "WARNING: Connection to http://$HOST (port 80) also fails."
        echo
        echo "This suggests that $HOST resolves to some IP address that does not reach this "
        echo machine where you are installing discourse.
      fi
      echo
      echo "The first thing to do is confirm that $HOST resolves to the IP address of this server."
      echo You usually do this at the same place you purchased the domain.
      echo
      echo If you are sure that the IP address resolves correctly, it could be a firewall issue.
      echo A web search for  \"open ports YOUR CLOUD SERVICE\" might help.
      echo
      echo This tool is designed only for the most standard installations. If you cannot resolve
      echo the issue above, you will need to edit containers/app.yml yourself and then type
      echo
      echo                   ./launcher rebuild app
      echo
      exit 1
      ;;
    2)
      echo "Continuing without port check."
      ;;
  esac
}

##
## Do we have docker?
##
check_and_install_docker () {
  docker_path=`which docker.io || which docker`
  if [ -z $docker_path ]; then
    read  -p "Docker not installed. Enter to install from https://get.docker.com/ or Ctrl+C to exit"
    curl https://get.docker.com/ | sh
  fi
  docker_path=`which docker.io || which docker`
  if [ -z $docker_path ]; then
    echo Docker install failed. Quitting.
    exit
  fi
}

##
## What are we running on
##
check_OS() {
  echo `uname -s`
}

##
## OS X available memory
##
check_osx_memory() {
  echo `free -m | awk '/Mem:/ {print $2}'`
}

##
## Linux available memory
##
check_linux_memory() {
  ## some VMs report just under 1GB of RAM, so
  ## make an exception and allow those with more
  ## than 989MB
  mem=`free -m --si | awk ' /Mem:/ {print $2}'`
  if [ "$mem" -ge 990 -a "$mem" -lt 1000 ]; then
    echo 1
  else
    echo `free -g --si | awk ' /Mem:/  {print $2} '`
  fi
}

##
## Do we have enough memory and disk space for Discourse?
##
check_disk_and_memory() {

  os_type=$(check_OS)
  avail_mem=0
  if [ "$os_type" == "Darwin" ]; then
    avail_mem=$(check_osx_memory)
  else
    avail_mem=$(check_linux_memory)
  fi

  if [ "$avail_mem" -lt 1 ]; then
    echo "WARNING: Discourse requires 1GB RAM to run. This system does not appear"
    echo "to have sufficient memory."
    echo
    echo "Your site may not work properly, or future upgrades of Discourse may not"
    echo "complete successfully."
    exit 1
  fi

  if [ "$avail_mem" -le 2 ]; then
    total_swap=`free -g --si | awk ' /Swap:/  {print $2} '`

    if [ "$total_swap" -lt 2 ]; then
      echo "WARNING: Discourse requires at least 2GB of swap when running with 2GB of RAM"
      echo "or less. This system does not appear to have sufficient swap space."
      echo
      echo "Without sufficient swap space, your site may not work properly, and future"
      echo "upgrades of Discourse may not complete successfully."
      echo
      echo "Ctrl+C to exit or wait 5 seconds to have a 2GB swapfile created."
      sleep 5

      ##
      ## derived from https://meta.discourse.org/t/13880
      ##
      install -o root -g root -m 0600 /dev/null /swapfile
      fallocate -l 2G /swapfile
      mkswap /swapfile
      swapon /swapfile
      echo "/swapfile       swap    swap    auto      0       0" | tee -a /etc/fstab
      sysctl -w vm.swappiness=10
      echo 'vm.swappiness = 10' > /etc/sysctl.d/30-discourse-swap.conf

      total_swap=`free -g --si | awk ' /Swap:/ {print $2} '`
      if [ "$total_swap" -lt 2 ]; then
        echo "Failed to create swap: are you root? Are you running on real hardware, or a fully virtualized server?"
        exit 1
      fi

    fi
  fi


  free_disk="$(df /var | tail -n 1 | awk '{print $4}')"
  if [ "$free_disk" -lt 5000 ]; then
    echo "WARNING: Discourse requires at least 5GB free disk space. This system"
    echo "does not appear to have sufficient disk space."
    echo
    echo "Insufficient disk space may result in problems running your site, and"
    echo "may not even allow Discourse installation to complete successfully."
    echo
    echo "Please free up some space, or expand your disk, before continuing."
    echo
    echo "Run \`apt-get autoremove && apt-get autoclean\` to clean up unused"
    echo "packages and \`./launcher cleanup\` to remove stale Docker containers."
    exit 1
  fi

}

##
## Check requirements before creating a copy of a config file we won't edit
##
check_root
check_and_install_docker
check_disk_and_memory

